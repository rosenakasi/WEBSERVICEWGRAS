

package org.maps.wgis;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;
import org.maps.wgis.model.TableColumName;

public class HIMSModelUtils {

	/**
	 * 1. A volatile variable can be used as an alternative way of achieving
	 * synchronization in Java in some cases, like Visibility. with volatile
	 * variable, it's guaranteed that all reader thread will see updated value
	 * of the volatile variable once write operation completed, without volatile
	 * keyword different reader thread may see different values.
	 * 
	 * 
	 * 2. volatile variable can be used to inform the compiler that a particular
	 * field is subject to be accessed by multiple threads, which will prevent
	 * the compiler from doing any reordering or any kind of optimization which
	 * is not desirable in a multi-threaded environment. Without volatile
	 * variable compiler can re-order the code, free to cache value of volatile
	 * variable instead of always reading from main memory
	 * 
	 */
	private static volatile HIMSModelUtils _instance;

	public static HIMSModelUtils getInstance() {
		if (_instance == null) {
			/**
			 * synchronized synchronizes the value of all variable between
			 * thread memory and "main" memory and locks and releases a monitor
			 * to boot
			 */
			synchronized (HIMSModelUtils.class) {
				if (_instance == null)
					_instance = new HIMSModelUtils();
			}
		}
		return _instance;
	}

	private HIMSModelUtils() {
	}

	public JSONObject patientTableStructureObject() {
		String layerName = "patients";
		String layerTitle = "patients";
		String category = "patients";
		String description = "This represents the patients table structure";
		List<TableColumName> fields = new ArrayList<TableColumName>();
		fields.add(new TableColumName("id", "String"));
		fields.add(new TableColumName("dateCreated", "Date"));
		fields.add(new TableColumName("dateChanged", "Date"));
		fields.add(new TableColumName("dateOfBirth", "Date"));
		fields.add(new TableColumName("firstName", "String"));
		fields.add(new TableColumName("lastName", "String"));
		fields.add(new TableColumName("phoneNumber", "String"));
		fields.add(new TableColumName("patientId", "String"));
		fields.add(new TableColumName("nwscMeterNumber", "String"));
		fields.add(new TableColumName("placeName", "String"));
		fields.add(new TableColumName("longitude", "String"));
		fields.add(new TableColumName("latitude", "String"));
		fields.add(new TableColumName("villageDesc", "String"));
		return GISUtils.getInstance().createTableJsonObject(layerName, layerTitle, category, description, fields);
	}
}
