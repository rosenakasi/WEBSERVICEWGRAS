
package org.maps.wgis.cookiehandler;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CustomCookieHandler {
	private static volatile CustomCookieHandler _instance;

	public static CustomCookieHandler getInstance() {
		if (_instance == null) {
			/**
			 * synchronized synchronizes the value of all variable between
			 * thread memory and "main" memory and locks and releases a monitor
			 * to boot
			 */
			synchronized (CustomCookieHandler.class) {
				if (_instance == null)
					_instance = new CustomCookieHandler();
			}
		}
		return _instance;
	}

	private CustomCookieHandler() {
	}

	/**
	 * 
	 * This method is used to get cookie details from an {@HttpURLConnection}
	 * 
	 * @param request
	 * @return
	 */
	private List<CookieModel> getCookieInformation(String urlPath) {
		try {
			URL url = new URL(urlPath);
			HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
			Map<String, List<String>> values = httpURLConnection.getHeaderFields();
			List<CookieModel> cookieModels = new ArrayList<CookieModel>();
			System.out.println("Header information to be set as cookie values\n");
			for (Map.Entry<String, List<String>> entry : values.entrySet()) {
				String key = String.valueOf(entry.getKey());
				if (key != null && !key.equalsIgnoreCase("null")) {
					System.out.println(key + " = " + entry.getValue());
					cookieModels.add(new CookieModel(key, entry.getValue()));
				}
			}
			return cookieModels;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	public String processCookie(String urlPath) {
		List<CookieModel> cookieModels = getCookieInformation(urlPath);
		StringBuilder stringBuilder = new StringBuilder();
		if (cookieModels != null) {
			for (CookieModel cookieModel : cookieModels) {
				stringBuilder.append(cookieModel.key + "=" + cookieModel.value + ";");
			}
		}
		System.out.println("\nComposed cookie : " + stringBuilder + "\n");
		return String.valueOf(stringBuilder);
	}

}
