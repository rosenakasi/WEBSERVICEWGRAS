
package org.maps.wgis.model;

import java.io.Serializable;

public class TableColumName implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String name;
	public String attributeType;

	/**
	 * @param name
	 * @param attributeType
	 */
	public TableColumName(String name, String attributeType) {
		super();
		this.name = name;
		this.attributeType = attributeType;
	}

}
