
package org.maps.wgis.model;

import java.io.Serializable;

public class WorkSpace implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public String name;
	public String type;
	public String workspaceId;

	/**
	 * @param name
	 * @param type
	 * @param workspaceId
	 */
	public WorkSpace(String name, String type, String workspaceId) {
		super();
		this.name = name;
		this.type = type;
		this.workspaceId = workspaceId;
	}

}
