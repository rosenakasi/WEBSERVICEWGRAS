
 
package org.maps.wgis;

import org.json.JSONObject;

public class TestWebservice {

	public static void main(String[] args) {
		System.out.println("Testing....");
		 new TestWebservice().createTable();
		 //new TestWebservice().getPublicWorkSpace();

		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * This method is used to fetch the public workspace , this returns a
	 * {@link JSONObject}
	 * 
	 * 
	 */
	public void getPublicWorkSpace() {
		try {
			String response = GISUtils.getInstance().sendGetRequest(GISUtils.WORKSPACE_URL);
			GISUtils.getInstance().getWorkSpacesFromJsonResponse(response);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 
	 * This method is used to create a point on the provided server however the
	 * provided url is not found
	 */
	public void createTable() {
		try {
			GISUtils.getInstance().sendPostRequest(GISUtils.CREATE_POINT_URL,
					HIMSModelUtils.getInstance().patientTableStructureObject());
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
