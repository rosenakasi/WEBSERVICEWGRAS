
/**
 *Feb 4, 2018
 *org.aa
 *mobility-services
 *INSPIRON
 *
 *UMAR K
 *
 * 
 */
package org.maps.wgis;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONArray;
import org.json.JSONObject;
import org.maps.wgis.model.TableColumName;
import org.maps.wgis.model.WorkSpace;

public class GISUtils {

	/**
	 * 1. A volatile variable can be used as an alternative way of achieving
	 * synchronization in Java in some cases, like Visibility. with volatile
	 * variable, it's guaranteed that all reader thread will see updated value
	 * of the volatile variable once write operation completed, without volatile
	 * keyword different reader thread may see different values.
	 * 
	 * 
	 * 2. volatile variable can be used to inform the compiler that a particular
	 * field is subject to be accessed by multiple threads, which will prevent
	 * the compiler from doing any reordering or any kind of optimization which
	 * is not desirable in a multi-threaded environment. Without volatile
	 * variable compiler can re-order the code, free to cache value of volatile
	 * variable instead of always reading from main memory
	 * 
	 */
	private static volatile GISUtils _instance;

	public static GISUtils getInstance() {
		if (_instance == null) {
			/**
			 * synchronized synchronizes the value of all variable between
			 * thread memory and "main" memory and locks and releases a monitor
			 * to boot
			 */
			synchronized (GISUtils.class) {
				if (_instance == null)
					_instance = new GISUtils();
			}
		}
		return _instance;
	}

	private GISUtils() {
	}

	public static final String WORK_SPACE_ID = "e87db6f8-861b-4a10-b5f6-3cbbb2bb04d3";
	public static final String LOGIN_URL = "https://wgras.gis.lu.se/login";
	public static final String WORKSPACE_URL = "https://wgras.gis.lu.se/temp/workspaces/writable";
	public static final String CREATE_POINT_URL = "https://wgras.gis.lu.se/temp/layer/point/empty";
	public static final String USERNAME = "nakasiRose";
	public static final String PASSWORD = "gNakasiRose123";

	/**
	 * 
	 * This method is used to create a JSONObject table structure to be sent to
	 * the api server
	 * 
	 * @param layerName
	 * @param layerTitle
	 * @param workspaceId
	 * @param category
	 * @param description
	 * @param fields
	 * @return
	 */
	public JSONObject createTableJsonObject(String layerName, String layerTitle, String category, String description,
			List<TableColumName> fields) {
		try {
			JSONObject tableJsonObject = new JSONObject();
			tableJsonObject.put("layerName", layerName);
			tableJsonObject.put("layerTitle", layerTitle);
			tableJsonObject.put("workspaceId", WORK_SPACE_ID);
			tableJsonObject.put("category", category);
			tableJsonObject.put("description", description);
			JSONArray fieldsArray = new JSONArray();
			for (TableColumName columName : fields) {
				JSONObject columNameJsonObject = new JSONObject();
				columNameJsonObject.put("name", columName.name);
				columNameJsonObject.put("attributeType", columName.attributeType);
				fieldsArray.put(columNameJsonObject);
			}
			tableJsonObject.put("fields", fieldsArray);

			return tableJsonObject;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public List<WorkSpace> getWorkSpacesFromJsonResponse(String jsonFile) {
		try {
			ArrayList<WorkSpace> workSpaces = new ArrayList<WorkSpace>();
			if (jsonFile != null) {
				JSONObject jsonObj = new JSONObject(jsonFile);
				if (jsonFile.contains("workspaces")) {
					JSONArray jsonArray = jsonObj.getJSONArray("workspaces");
					for (int i = 0; i < jsonArray.length(); i++) {
						JSONObject recipientJsonObj = jsonArray.getJSONObject(i);
						String jsonString = String.valueOf(recipientJsonObj);
						String name = null;
						String type = null;
						String workspaceId = null;
						if (jsonString.contains("name"))
							name = recipientJsonObj.getString("name");
						if (jsonString.contains("type"))
							type = recipientJsonObj.getString("type");
						if (jsonString.contains("workspaceId"))
							workspaceId = recipientJsonObj.getString("workspaceId");
						System.out.println(" name        : " + name);
						System.out.println(" type        : " + type);
						System.out.println(" workspaceId : " + workspaceId);
						System.out.println("-----------------------------------");
						workSpaces.add(new WorkSpace(name, type, workspaceId));
					}
				}
			}

			return workSpaces;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	/**
	 * 
	 * This method is used to get a JSON response from the server
	 * 
	 * @param requestUrl
	 * @return
	 * @throws Exception
	 */
	public String sendGetRequest(String requestUrl) throws Exception {
		URL obj = new URL(requestUrl);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		int responseCode = con.getResponseCode();
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		System.out.println(response);
		return String.valueOf(response);

	}

	/**
	 * 
	 * This method is used to post a {@link JSONObject} request to the server
	 * and get the returned object
	 * 
	 * @param requestUrl
	 * @param jsonObject
	 * @return
	 * @throws Exception
	 */
	public String sendPostRequest(String requestUrl, JSONObject jsonObject) throws Exception {
		byte[] postDataBytes = String.valueOf(jsonObject).getBytes("UTF-8");
		URL obj = new URL(requestUrl);
		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
		con.setRequestMethod("POST");
		con.setRequestProperty("Content-Type", "application/json");
		con.setDoOutput(true);
		con.getOutputStream().write(postDataBytes);
		int responseCode = con.getResponseCode();
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		System.out.println(response.toString());
		return String.valueOf(response);
	}

}
